create table temp as
  select a.articleid, substring_index(substring_index(a.image_urls,',',b.help_topic_id+1),',',-1) as 'SURL'
    from  Articles a
    join  mysql.help_topic b on b.help_topic_id < (length(a.image_urls) - length(replace(a.image_urls,',',''))+1);
  
  
create table uni as 
  select t.articleid, concat(concat(t.surl, ':'), i.feature)
    from temp t, ImageFeatures i 
    where t.surl = i.url;
  

create table ArticleFeatures as 
  select articleid, surl = (stuff((select ',' + surl from uni 
    where articleid = a.articleid for xml path('')),1,1,'')) as "url_features" from uni a group by articleid;
  
  
